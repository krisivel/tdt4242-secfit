stages:
  - test
  - codequality
  - deploy

django-test:
  stage: test
  image: python:3.8-alpine
  script:
    - cd backend/secfit/
    - pip install -r requirements.txt
    - python manage.py test

bandit-test:
  stage: test
  image: python:3.8-alpine
  before_script:
    - pip install --upgrade bandit
  script:
    - bandit -r backend/secfit/

eslint:
 stage: test
 image: node
 script:
    - cd frontend
    - npm install
    - npm run lint

njsscan:
  stage: test
  image: python:3.8-alpine
  before_script:
    - pip install --upgrade njsscan
  script:
    - cd frontend
    - njsscan www

flake8:
  stage: test
  image: python:3.8-alpine
  allow_failure: true
  before_script:
    - apk add jq
    - pip install --upgrade flake8 flake8-django flake8-bugbear flake8-builtins flake8-eradicate flake8-mutable flake8-pie flake8-return flake8-requirements flake8-simplify flake8-bandit dlint flake8-string-format flake8-cognitive-complexity flake8-fixme flake8-codeclimate
    - pip install flake8-functions typing_extensions mypy_extensions
    - mkdir public
  script:
    - cd backend/secfit/
    - flake8 --version
    - args="--max-line-length=120 --ignore=W1,W2,W3,W5,E1,E2,E3,E5,I900,R504,S104"
    - args="$args --per-file-ignores=users/tests.py:S105"
    - flake8 $args --exit-zero --format=codeclimate . | jq -n '[ inputs ]' > ../../public/flake8-codeclimate.json
    - flake8 $args
  artifacts:
    paths:
      - public
    reports:
      codequality: public/flake8-codeclimate.json
    when: always

pylint:
  stage: test
  image: python:3.8-alpine
  allow_failure: true
  before_script:
    - apk add jq
    - mkdir public
    - cd backend/secfit/
    - pip install --upgrade pylint pylint_django pylint-gitlab
    - pip install -r requirements.txt
  script:
    - docstring=C0112,C0114,C0115,C0116
    - names=C0103,C0104,C0144,W0111
    - other="duplicate-code,too-many-ancestors" # duplicate-code: only reported false positives in this project, disabling
    - args="-d $docstring,$names,$other,format,spelling --load-plugins pylint_django"
    - pylint $args --output-format=pylint_gitlab.GitlabCodeClimateReporter **/*.py > ../../public/pylint-codeclimate.json || echo
    - pylint $args **/*.py
  artifacts:
    paths:
      - public
    reports:
      codequality: public/pylint-codeclimate.json
    when: always

codequality_merger:
  stage: codequality
  image: alpine:latest
  before_script:
    - apk add jq
  script:
    - jq -n '[ inputs ] | add' public/flake8-codeclimate.json public/pylint-codeclimate.json > public/gl-code-quality-report.json
  needs:
    - job: flake8
      artifacts: true
    - job: pylint
      artifacts: true
  artifacts:
    paths:
      - public
    reports:
      codequality: public/gl-code-quality-report.json
    when: always

deploy:
  image: alpine:latest
  stage: deploy
  tags:
    - deployment
  script:
    - chmod og= $SSH_DEPLOY_KEY
    - apk update && apk add openssh-client
    - alias ssh="ssh -i $SSH_DEPLOY_KEY -o StrictHostKeyChecking=no $SSH_DEPLOY_USER@$SSH_DEPLOY_IP"
    - ssh "cd tdt4242-secfit && git fetch --all"
    - ssh "cd tdt4242-secfit && git checkout $CI_COMMIT_SHA"
    - ssh "cd tdt4242-secfit && docker-compose pull"
    - ssh "cd tdt4242-secfit && docker-compose build"
    - ssh "cd tdt4242-secfit && docker-compose up -d"
  only:
    - master
  needs:
    - django-test
