def stringify_model(model):
    """
    Stringify any class with field names and values. Useful default for stringifying django data models.

    E.g.:

            class Book():
                def __init__(self, title, author):
                    self.title = title
                    self.author = author

            book = Book('Clockwork_Planet Volume 1', 'Kurokkuwāku Puranetto')
            print(stringify_model(book))
            # Book(title=Clockwork_Planet Volume 1, author=Kurokkuwāku Puranetto)
    """
    return type(model).__name__ + '(' + ', '.join([f'{k}={v}' for k, v in model.__dict__.items()]) + ')'
