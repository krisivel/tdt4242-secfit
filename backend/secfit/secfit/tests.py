"""
Blackbox tests
"""
import itertools
import json
from datetime import datetime as dt

from django.test import TestCase
from django.utils.timezone import pytz
from rest_framework.test import (
    APIRequestFactory, RequestsClient, force_authenticate)
from rest_framework_simplejwt.tokens import RefreshToken

from comments.models import Comment
from comments.views import CommentList

from users.models import User
from users.serializers import UserSerializer
from users.views import UserList
from workouts.models import (Exercise, ExerciseInstance, Template, Workout,
                             WorkoutFile)
from workouts.views import (WorkoutList)


def _get_valid_user():
    return {
        'username': 'user-1',
        'email': 'user1@example.com',
        'password': '1234567890abc',
        'password1': '1234567890abc',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
        'athletes': [],
        'workouts': [],
        'coach_files': [],
        'athlete_files': [],
    }


# Boundary values are based on any and all documentation we could find for this application in the tasks given out to us
# Some values might therefor seem strange (e.g. exercies can have negative numbers), but we found no documentation stating otherwise
#  so we assume this to be a consious choise to accept values like these.
# We are after all only testing the application at this point, not deciding what the test requirements should be.
class BoundaryTestsRegister(TestCase):
    """
    1-way testing

    Boundaries:
    * username:
            * min- = 0
            * min = 1
            * nom = 5
            * max = 150
            * max+ = 151
    * email:
            * min- = invalid email
            * nom = valid email
    * pass*:
            * 12 <= len
            * min- = 11
            * min = 12
            * nom = 15
    * phone:
            * nom: any
    * country:
            * nom: any
    * city:
            * nom: any
    * street address
            * nom: any
    * athletes:
            * nom: any
    * workouts:
            * nom: any
    * coach_files:
            * nom: any
    * athlete_files:
            * nom: any
    """

    def _test_values(self, field, values, status_code=201):
        view = UserList.as_view()
        factory = APIRequestFactory()
        data = _get_valid_user()
        for value, valid in values.items():
            User.objects.filter().delete()
            data[field] = value
            if field == 'password':
                data['password1'] = value
            request = factory.post('/api/users/', data, 'json')
            response = view(request)
            if 'render' in dir(response):
                response.render()
            self.assertEqual(response.status_code == status_code, valid)

    def test_username(self):
        """Test boundary values for username field on user registration"""
        usernames = {
            'a' * 0: False,    # min-
            'a' * 1: True,     # min
            'a' * 5: True,     # nom
            'a' * 150: True,   # max
            'a' * 151: False,  # max+
        }
        self._test_values('username', usernames)

    def test_email(self):
        """Test boundary values for email field on user registration"""
        emails = {
            'invalid email': False,   # min-
            'valid@email.com': True,  # min
        }
        self._test_values('email', emails)

    def test_password(self):
        """Test boundary values for password field on user registration"""
        passwords = {
            '1234567890a': False,     # min-
            '1234567890ab': True,     # min
            '1234567890abcde': True,  # nom
        }
        self._test_values('password', passwords)


class BoundaryTestsWorkout(TestCase):
    """
    1-way testing

    Boundaries:
    * name:
            * min- = len 0
            * min = len 1
            * norm = 5
    * date:
            * min- = invalid date
            * nom = valid date
    * notes:
            * nom = any
    exercise_instances:
            * nom = any list length
    exercise_instances-obj:
            * exercise:
                    * min- = invalid primary key
                    * nom = valid primary key
            * number:
                    * min- = invalid number
                    * nom = valid number
            * sets:
                    * min- = invalid number
                    * nom = valid number

    """

    def setUp(self):
        self.user = User.objects.create(username='test_user')
        self.ex = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')

    def _get_ex(self):
        return f'/api/exercises/{self.ex.id}/'

    def _get_valid_workout(self):
        return {
            'name': 'workout',
            'date': '0001-01-01T00:00:00.000Z',
            'notes': 'note',
            'exercise_instances': [{
                    'exercise': self._get_ex(),
                    'number': 0,
                'sets': 0,
            }],
        }

    def _test_values(self, field, values, status_code=201):
        view = WorkoutList.as_view()
        factory = APIRequestFactory()
        data = self._get_valid_workout()
        items = values
        if isinstance(items, dict):
            items = items.items()
        for value, valid in items:
            data[field] = value
            request = factory.post('/api/workouts/', data, 'json')
            force_authenticate(request, user=self.user)
            response = view(request)
            if 'render' in dir(response):
                response.render()
            self.assertEqual(response.status_code == status_code, valid)

    def test_name(self):
        """Test boundary values for name field on workout registration"""
        names = {
            'a' * 0: False,  # min-
            'a' * 1: True,   # min
            'a' * 5: True,   # norm
        }
        self._test_values('name', names)

    def test_date(self):
        """Test boundary values for date field on workout registration"""
        dates = {
            'not a date': False,           # min-
            '0001-01-01T00:00:00Z': True,  # min
        }
        self._test_values('date', dates)

    def test_exercise_id(self):
        """Test boundary values for exercise id field on workout registration"""
        exercise_instances = [
            ([{'exercise': 'invalid pk', 'number': 0, 'sets': 0}], False),   # min-
            ([{'exercise': self._get_ex(), 'number': 0, 'sets': 0}], True),  # min
        ]
        self._test_values('exercise_instances', exercise_instances)

    def test_exercise_number(self):
        """Test boundary values for exercise number field on workout registration"""
        exercise_instances = [
            ([{'exercise': self._get_ex(), 'number': 'not a number', 'sets': 0}], False),  # min-
            ([{'exercise': self._get_ex(), 'number': -1, 'sets': 0}], True),               # min
        ]
        self._test_values('exercise_instances', exercise_instances)

    def test_exercise_sets(self):
        """Test boundary values for exercise sets field on workout registration"""
        exercise_instances = [
            ([{'exercise': self._get_ex(), 'number': 1, 'sets': 'not a number'}], False),  # min-
            ([{'exercise': self._get_ex(), 'number': 1, 'sets': -1}], True),               # min
        ]
        self._test_values('exercise_instances', exercise_instances)


# System and integration tests will focus on that we can successfully send external network requests
#  to the application (/api/ endpoint) and get valid responses. The individual features already have
#  heavy unit testing on them, so we will not go into detail for testing valid input and so on,
#  rather test that the modules in the system talk correctly to each other. This means we are testing
#  the links between the modules and dataflow, not that the end modules are working (as this is covered)
#  by unit tests for the respective modules.
#
# We use the RequestsClient framework as this sends actual network requests to the application using the
#  standard `request` framework (compared to our fake requests with APIRequestFactory).
class SystemAndIntegrationTests(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='a')
        self.user.save()
        self.today = dt.today()
        self.workout = Workout.objects.create(name='workout', date=dt(self.today.year, self.today.month, self.today.day, tzinfo=pytz.UTC), owner=self.user, visibility='PU')
        self.type = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')
        self.ex = ExerciseInstance.objects.create(workout=self.workout, exercise=self.type, sets=5, number=5)
        self.template = Template.objects.create(name='template', owner=self.user, visibility='PU')
        self.client = RequestsClient()
        self._login()

    def _login(self):
        token = RefreshToken.for_user(self.user)  # create a new login token, same as logging the user in
        self.client.headers.update({'Authorization': f'Bearer {token.access_token}'})

    def test_statistics(self):
        """Test that an external request can access statistics"""
        days = 30
        resp = self.client.get(f'http://testserver/api/stats/?days={days}/')
        data = resp.json()
        self.assertEqual(len(data), 1)
        self.assertTrue(data[0]['unit'], self.type.name)
        self.assertTrue(data[0]['sets'], self.ex.sets)
        self.assertTrue(data[0]['reps'], self.ex.sets * self.ex.number)

    def test_calender(self):
        """Test that an external request can access the calender"""
        month = str(self.today.year).zfill(4) + '-' + str(self.today.month).zfill(2)
        resp = self.client.get(f'http://testserver/api/calender/?month={month}')
        data = resp.json()
        self.assertEqual(data['count'], 1)
        data = data['results'][0]
        self.assertTrue(data['name'], self.workout.name)

    def test_templates(self):
        """Test that an external request can access the template list view"""
        resp = self.client.get('http://testserver/api/templates/')
        data = resp.json()
        self.assertEqual(data['count'], 1)
        data = data['results'][0]
        self.assertTrue(data['name'], self.template.name)


class TwoWayTesting(TestCase):

    def test_two_way(self):  # noqa: CCR001
        """Perform a two way test on the following variables: username, password, email

        * Domains for username: 
                * empty
                * <= 150
                * > 150
        * Domains for password: 
                * < 12 
                * >= 12
        * Domains for email: 
                * invalid email
                * valid email
        """

        tests = [
            {
                'field': 'username',
                'values': {
                    'a' * 0: False,
                    'a' * 5: True,
                    'a' * 200: False,
                }
            },
            {
                'field': 'password',
                'values': {
                    '': False,
                    '1234567890abc': True,
                },
            },
            {
                'field': 'email',
                'values': {
                    'invalid email': False,
                    'valid@email.com': True,
                }
            }
        ]

        pairs = itertools.combinations(tests, 2)

        def update(data, key, value):
            data[key] = value
            if key == 'password':
                data['password1'] = value

        for test1, test2 in pairs:
            for value1, valid1 in test1['values'].items():
                for value2, valid2 in test2['values'].items():
                    user = _get_valid_user()
                    update(user, test1['field'], value1)
                    update(user, test2['field'], value2)

                    validator = UserSerializer(data=user)
                    valid = validator.is_valid()
                    if valid != (valid1 & valid2):
                        print('error:', value1, valid1, value2, valid2)
                    self.assertEqual(valid, valid1 & valid2)


# pylint: disable=R0902 too-many-instance-attributes
class FR5_Testsing(TestCase):

    def setUp(self):
        self.coach = User.objects.create(username='coach')
        self.user = User.objects.create(username='user', coach=self.coach)
        self.other = User.objects.create(username='other')

        self.pub_w = Workout.objects.create(name='pub', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='PU')
        self.coach_w = Workout.objects.create(name='coach', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='CO')
        self.priv_w = Workout.objects.create(name='priv', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='PR')

        self.pub_f = WorkoutFile.objects.create(workout=self.pub_w, owner=self.user)
        self.coach_f = WorkoutFile.objects.create(workout=self.coach_w, owner=self.user)
        self.priv_f = WorkoutFile.objects.create(workout=self.priv_w, owner=self.user)

        self.pub_c = Comment.objects.create(owner=self.user, workout=self.pub_w, content='comment')
        self.coach_c = Comment.objects.create(owner=self.user, workout=self.coach_w, content='comment')
        self.priv_c = Comment.objects.create(owner=self.user, workout=self.priv_w, content='comment')

        self.type = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')

        self.pub_ex = ExerciseInstance.objects.create(workout=self.pub_w, exercise=self.type, sets=5, number=10)
        self.coach_ex = ExerciseInstance.objects.create(workout=self.coach_w, exercise=self.type, sets=5, number=10)
        self.priv_ex = ExerciseInstance.objects.create(workout=self.priv_w, exercise=self.type, sets=5, number=10)

        self.users = [self.coach, self.user, self.other]

    def _get_view_matrix(self, views):
        """Returns a matrix containing what each type of user should have permission to view"""
        pu, co, pr = views
        return {
            self.user: [pu, co, pr],
            self.coach: [pu, co],
            self.other: [pu]
        }

    # pylint: disable=R0201 no-self-use
    def _request(self, user, view):
        factory = APIRequestFactory()
        request = factory.get('/')
        force_authenticate(request, user=user)
        response = view(request)
        if 'render' in dir(response):
            response.render()
        return json.loads(response.content)

    def test_view_workout(self):
        views = [self.pub_w, self.coach_w, self.priv_w]
        matrix = self._get_view_matrix(views)

        render = WorkoutList().as_view()

        for user in self.users:
            data = {workout['name']: len(workout['files']) for workout in self._request(user, render)['results']}
            for view in views:
                self.assertEqual(view.name in data, view in matrix[user])
                if view.name in data:
                    self.assertEqual(data[view.name], 1)  # can view files

    def test_view_comments(self):
        views = [self.pub_c, self.coach_c, self.priv_c]
        matrix = self._get_view_matrix(views)

        render = CommentList().as_view()

        for user in self.users:
            data = [comment['id'] for comment in self._request(user, render)['results']]
            for view in views:
                self.assertEqual(view.id in data, view in matrix[user])
