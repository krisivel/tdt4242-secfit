from django.contrib.auth import get_user_model
from rest_framework import permissions


class IsCurrentUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


def extract_athlete_id(url):
    """
    Extract the athlete id from an athelete url
    """
    return url.split("/")[-2]


class IsAthlete(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method != "POST":
            return True
        if request.data.get("athlete"):
            athlete_id = extract_athlete_id(request.data["athlete"])
            return athlete_id == request.user.id
        return False

    def has_object_permission(self, request, view, obj):
        return request.user == obj.athlete


class IsCoach(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method != "POST":
            return True
        if request.data.get("athlete"):
            athlete_id = extract_athlete_id(request.data["athlete"])
            athlete = get_user_model().objects.get(pk=athlete_id)
            return athlete.coach == request.user
        return False

    def has_object_permission(self, request, view, obj):
        return request.user == obj.athlete.coach
