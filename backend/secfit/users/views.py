import base64
import json

from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.signing import Signer
from django.db.models import Q
from rest_framework import generics, mixins, permissions
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework_simplejwt.tokens import RefreshToken
from users.models import AthleteFile, Offer, User
from users.permissions import IsAthlete, IsCoach, IsCurrentUser
from users.serializers import (AthleteFileSerializer, OfferSerializer,
                               UserGetSerializer, UserPutSerializer,
                               UserSerializer)
from workouts.mixins import CreateListModelMixin
from workouts.permissions import IsOwner, IsReadOnly


class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = UserSerializer
    users = []
    admins = []

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        status = self.request.query_params.get("user", None)
        if self.request.user and status == "current":
            return get_user_model().objects.filter(pk=self.request.user.pk)
        return get_user_model().objects.all()


class UserDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    lookup_field_options = ["pk", "username"]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        self.serializer_class = UserPutSerializer
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class OfferList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.request.user:
            # filtering by status (if provided)
            status = self.request.query_params.get("status", None)
            if status is not None:
                qs = Offer.objects.filter(
                    (Q(owner=self.request.user) | Q(recipient=self.request.user)) & Q(status=status)
                ).distinct()
            else:
                qs = Offer.objects.filter(Q(owner=self.request.user)).distinct()

            # filtering by category (sent or received, if provided)
            cat = self.request.query_params.get("category", None)
            if cat == "sent":
                qs = qs.filter(owner=self.request.user)
            elif cat == "received":
                qs = qs.filter(recipient=self.request.user)
            return qs

        return Offer.objects.none()


class OfferDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class AthleteFileList(mixins.ListModelMixin, mixins.CreateModelMixin, CreateListModelMixin, generics.GenericAPIView):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsCoach)]
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.request.user:
            return AthleteFile.objects.filter(Q(athlete=self.request.user) | Q(owner=self.request.user)).distinct()

        return AthleteFile.objects.none()


class AthleteFileDetail(mixins.RetrieveModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsOwner)]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


def sign_data(data):
    signer = Signer()
    return signer.sign(data)


class RememberMe(generics.GenericAPIView):
    """
    Allow users to save a persistent session in their browser
    """

    def get(self, request):
        if not request.user.is_authenticated:
            raise PermissionDenied
        creds = [self.request.user.id, sign_data(self.request.user.id)]
        cookie = base64.b64encode(json.dumps(creds).encode())
        return Response({"remember_me": cookie})

    def post(self, request):
        cookie = request.COOKIES['remember_me']
        user_id, sign = json.loads(base64.b64decode(cookie))
        if sign != sign_data(user_id):
            raise ObjectDoesNotExist

        user = User.objects.get(pk=user_id)
        if user is None:
            raise ObjectDoesNotExist

        refresh = RefreshToken.for_user(user)
        return Response(
            {
                "refresh": str(refresh),
                "access": str(refresh.access_token),
            }
        )
