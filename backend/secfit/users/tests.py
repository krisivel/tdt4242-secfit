"""
Tests for user classes.
"""
from django.test import TestCase
from users.models import User
from users.serializers import UserSerializer

# As nothing else is mentioned in the assignment, we assume we only need to test the two functions of the UserSerializer class
# naimly validate_password() and create().

# In the UserSerializer several fields like 'phone_number' are optional and no validation takes place.
# As nothing else is documented, we assume this is a feature and that they should not be required.
# As there is only one method for validating the password field, we assume this is the only field needing validation.


def _valid_user():
    return {
        'username': 'test-user',
        'password': '1234567890ab',
        'password1': '1234567890ab',
        'email': '',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
        'athletes': [],
        'workouts': [],
        'coach_files': [],
        'athlete_files': [],
    }


class UserSerializerTests(TestCase):

    def test_valid(self):
        """Test a valid user"""
        ser = UserSerializer(data=_valid_user())
        self.assertTrue(ser.is_valid())

    def test_diff_passwords(self):
        """Both provided passwords must be equal"""
        user = _valid_user()
        user['password'] = '1234567890ab'  # nosec
        user['password1'] = '1234567890abc'
        ser = UserSerializer(data=user)
        self.assertFalse(ser.is_valid())

    def test_too_short_password(self):
        """Passwords must be minimum 12 characters"""
        user = _valid_user()
        user['password'] = '123abc'  # nosec
        user['password1'] = '123abc'
        ser = UserSerializer(data=user)
        self.assertFalse(ser.is_valid())

    def test_common_passwords(self):
        """Don't allow common passwords"""
        common = ["123456", "123456789", "picture1", "password", "12345678", "111111", "123123", "12345", "1234567890", "senha", "1234567", "qwerty", "abc123", "Million2", "000000", "1234", "iloveyou", "aaron431", "password1", "qqww1122"]
        user = _valid_user()
        for pw in common:
            user['password'] = pw
            user['password1'] = pw
            ser = UserSerializer(data=user)
            self.assertFalse(ser.is_valid())

    def test_create_user(self):
        """Test creation and saving of user"""
        data = _valid_user()
        ser = UserSerializer(data=data)
        self.assertTrue(ser.is_valid())
        ser.save()
        user = User.objects.get(username=data['username'])
        self.assertTrue(bool(user))
