"""
Contains views for the workouts application. These are mostly class-based views.
"""
from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.db.models import Q
from django.http import JsonResponse
from django.utils.timezone import pytz
from rest_framework import (filters, generics, mixins, permissions)
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from workouts.mixins import CreateListModelMixin
from workouts.models import (Exercise, ExerciseInstance, Template, TemplateExerciseInstance, TemplateWorkoutFile, Workout, WorkoutFile)
from workouts.parsers import MultipartJsonParser
from workouts.permissions import (IsCoachAndVisibleToCoach, IsCoachOfTemplateAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsOwner, IsOwnerOfTemplate, IsOwnerOfWorkout, IsPublic, IsReadOnly, IsTemplatePublic, IsWorkoutPublic)
from workouts.serializers import (ExerciseInstanceSerializer, ExerciseSerializer, TemplateExerciseInstanceSerializer, TemplateSerializer, TemplateWorkoutFileSerializer, WorkoutFileSerializer, WorkoutSerializer)


@api_view(["GET"])
def api_root(request, InFormat=None):
    return Response(
        {
            "users": reverse("user-list", request=request, format=InFormat),
            "workouts": reverse("workout-list", request=request, format=InFormat),
            "templates": reverse("template-list", request=request, format=InFormat),
            "exercises": reverse("exercise-list", request=request, format=InFormat),
            "exercise-instances": reverse("exercise-instance-list", request=request, format=InFormat),
            "template-exercise-instances": reverse("template-exercise-instance-list", request=request, format=InFormat),
            "workout-files": reverse("workout-file-list", request=request, format=InFormat),
            "template-workout-files": reverse("template-workout-file-list", request=request, format=InFormat),
            "comments": reverse("comment-list", request=request, format=InFormat),
            "likes": reverse("like-list", request=request, format=InFormat),
        }
    )


def filter_models(request, model_class, perms):
    view = None

    def test(workout):
        return any([perm.has_object_permission(request, view, workout) for perm in perms])

    return list(filter(test, model_class.objects.all()))


class WorkoutList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """Class defining the web response for the creation of a Workout, or displaying a list
    of Workouts

    HTTP methods: GET, POST
    """

    serializer_class = WorkoutSerializer
    # User must be authenticated to create/view workouts
    parser_classes = [MultipartJsonParser, JSONParser]  # For parsing JSON and Multi-part requests
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["name", "date", "owner__username"]
    permission_classes = [permissions.IsAuthenticated & (IsOwner | IsOwnerOfWorkout | (IsReadOnly & (IsCoachOfWorkoutAndVisibleToCoach | IsWorkoutPublic)))]
    model_class = Workout

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.request.user:
            # A workout should be visible to the requesting user if any of the following hold:
            # - The workout has public visibility
            # - The owner of the workout is the requesting user
            # - The workout has coach visibility and the requesting user is the owner's coach
            return filter_models(self.request, self.model_class, [IsPublic(), IsOwner(), IsCoachAndVisibleToCoach()])

        return []


class WorkoutDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    """Class defining the web response for the details of an individual Workout.

    HTTP methods: GET, PUT, DELETE
    """

    queryset = Workout.objects.all()
    serializer_class = WorkoutSerializer
    permission_classes = [permissions.IsAuthenticated & (IsOwner | (IsReadOnly & (IsCoachAndVisibleToCoach | IsPublic)))]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ExerciseList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """Class defining the web response for the creation of an Exercise, or
    a list of Exercises.

    HTTP methods: GET, POST
    """

    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ExerciseDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    """Class defining the web response for the details of an individual Exercise.

    HTTP methods: GET, PUT, PATCH, DELETE
    """

    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ExerciseInstanceList(mixins.ListModelMixin, mixins.CreateModelMixin, CreateListModelMixin, generics.GenericAPIView):

    serializer_class = ExerciseInstanceSerializer
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfWorkout]
    model_class = ExerciseInstance

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user:
            return filter_models(self.request, self.model_class, [IsWorkoutPublic(), IsOwnerOfWorkout(), IsCoachOfWorkoutAndVisibleToCoach()])

        return []


class ExerciseInstanceDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):

    serializer_class = ExerciseInstanceSerializer
    permission_classes = [permissions.IsAuthenticated & (IsOwnerOfWorkout | (IsReadOnly & (IsCoachOfWorkoutAndVisibleToCoach | IsWorkoutPublic)))]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class WorkoutFileList(mixins.ListModelMixin, mixins.CreateModelMixin, CreateListModelMixin, generics.GenericAPIView):

    serializer_class = WorkoutFileSerializer
    model_class = WorkoutFile
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfWorkout]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.request.user:
            return filter_models(self.request, self.model_class, [IsWorkoutPublic(), IsOwnerOfWorkout(), IsCoachOfWorkoutAndVisibleToCoach()])

        return []


class WorkoutFileDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):

    queryset = WorkoutFile.objects.all()
    serializer_class = WorkoutFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsOwner | IsOwnerOfWorkout | (IsReadOnly & (IsCoachOfWorkoutAndVisibleToCoach | IsWorkoutPublic)))]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class TemplateList(WorkoutList):
    """Class defining the web response for the creation of a Template, or displaying a list
    of Templates

    HTTP methods: GET, POST
    """

    serializer_class = TemplateSerializer
    model_class = Template
    permission_classes = [permissions.IsAuthenticated & (IsOwner | IsOwnerOfTemplate | (IsReadOnly & (IsCoachOfTemplateAndVisibleToCoach | IsTemplatePublic)))]


class TemplateDetail(WorkoutDetail):
    """Class defining the web response for the details of an individual Template.

    HTTP methods: GET, PUT, DELETE
    """

    queryset = Template.objects.all()
    serializer_class = TemplateSerializer


class TemplateExerciseInstanceList(ExerciseInstanceList):

    serializer_class = TemplateExerciseInstanceSerializer
    model_class = TemplateExerciseInstance
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfTemplate]

    def get_queryset(self):
        if self.request.user:
            return filter_models(self.request, self.model_class, [IsTemplatePublic(), IsOwnerOfTemplate(), IsCoachOfTemplateAndVisibleToCoach()])

        return []


class TemplateExerciseInstanceDetail(ExerciseInstanceDetail):

    serializer_class = ExerciseInstanceSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (
            IsOwnerOfWorkout
            | (IsReadOnly & (IsCoachOfTemplateAndVisibleToCoach | IsTemplatePublic))
        )
    ]


class TemplateWorkoutFileList(WorkoutFileList):

    serializer_class = TemplateWorkoutFileSerializer
    model_class = TemplateWorkoutFile
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfTemplate]

    def get_queryset(self):
        if self.request.user:
            return filter_models(self.request, self.model_class, [IsTemplatePublic(), IsOwnerOfTemplate(), IsCoachOfTemplateAndVisibleToCoach()])

        return []


class TemplateWorkoutFileDetail(WorkoutFileDetail):

    queryset = TemplateWorkoutFile.objects.all()
    serializer_class = TemplateWorkoutFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsOwner | IsOwnerOfTemplate | (IsReadOnly & (IsCoachOfTemplateAndVisibleToCoach | IsTemplatePublic)))]


class ExerciseStats(generics.GenericAPIView):

    permission_classes = [permissions.IsAuthenticated]

    # pylint: disable=W0613
    def get(self, request, *args, todayFunc=datetime.today, **kwargs):

        try:
            days = int(request.query_params.get('days'))
        except (KeyError, ValueError):
            days = 7

        today = todayFunc().astimezone(pytz.UTC)
        exercises = ExerciseInstance.objects.filter(Q(workout__owner=request.user) & Q(workout__date__lte=today) & Q(workout__date__gt=today - relativedelta(days=days))).distinct()

        def sumSets(ExType):
            return sum([ex.sets for ex in exercises if ex.exercise == ExType])

        def sumReps(ExType):
            return sum([ex.sets * ex.number for ex in exercises if ex.exercise == ExType])

        types = {ex.exercise for ex in exercises}

        stats = [{"unit": t.unit, "name": t.name, "sets": sumSets(t), "reps": sumReps(t)} for t in types]

        return JsonResponse(stats, safe=False)


class CalenderView(mixins.ListModelMixin, generics.GenericAPIView):
    """Class defining the web response for viewing the calender

    HTTP methods: GET, POST
    """

    serializer_class = WorkoutSerializer
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [JSONParser]
    filter_backends = [filters.OrderingFilter]

    ordering_fields = ["date", "name", "owner__username"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    # pylint: disable=W0613
    def post(self, request, *args, **kwargs):
        date = request.data['date']
        date = datetime.fromisoformat(date.replace('Z', '')).astimezone(pytz.UTC)
        name = request.data['name']
        Workout.objects.create(name=name, date=date, owner=request.user, visibility='PR')
        return JsonResponse({'msg': 'ok'})

    def get_queryset(self):
        qs = Workout.objects.none()
        if self.request.user:
            now = datetime.now()
            start = self.request.query_params.get('month', None)
            try:
                start = datetime(int(start.split('-')[0]), int(start.split('-')[1]), 1, tzinfo=pytz.UTC)
            except (KeyError, ValueError):
                start = datetime(now.year, now.month, 1, tzinfo=pytz.UTC)
            end = start + relativedelta(months=1)
            qs = Workout.objects.filter(Q(owner=self.request.user) & Q(date__gte=start) & Q(date__lt=end)).distinct()

        return qs
