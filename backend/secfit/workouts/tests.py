"""
Tests for the workouts application.
"""
import json
from datetime import datetime as dt

from django.test import TestCase
from django.utils.timezone import pytz
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory, force_authenticate
from users.models import User
from workouts import views
from workouts.models import Exercise, ExerciseInstance, Template, Workout
from workouts.permissions import (IsCoachAndVisibleToCoach,
                                  IsCoachOfWorkoutAndVisibleToCoach, IsOwner,
                                  IsOwnerOfWorkout, IsPublic, IsReadOnly,
                                  IsWorkoutPublic)


class StatisticsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='user')
        self.other = User.objects.create(username='other')

    # pylint: disable=R0201
    def _request(self, user, day, days):
        factory = APIRequestFactory()
        view = views.ExerciseStats.as_view()
        request = factory.get(f'/api/stats/?days={days}')
        force_authenticate(request, user=user)
        response = view(request, todayFunc=lambda: dt(*day, tzinfo=pytz.UTC))
        return json.loads(response.content)

    def test_time_interval(self):
        """Test that only workouts for the given time interval is returned"""
        workout = Workout.objects.create(name='workout', date=dt(2021, 1, 1, tzinfo=pytz.UTC), owner=self.user, visibility='PR')
        ExType = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')
        ExerciseInstance.objects.create(workout=workout, exercise=ExType, sets=1, number=1)

        data = self._request(self.user, day=(2021, 1, 5), days=3)
        self.assertEqual(len(data), 0)
        data = self._request(self.user, day=(2021, 1, 5), days=7)
        self.assertEqual(len(data), 1)

        data = self._request(self.user, day=(2020, 12, 31), days=30)
        self.assertEqual(len(data), 0)
        data = self._request(self.user, day=(2021, 1, 30), days=30)
        self.assertEqual(len(data), 1)

        data = self._request(self.user, day=(2022, 1, 1), days=366)
        self.assertEqual(len(data), 1)

    def test_sum(self):
        """Test that stats gets summed up"""
        Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')

        def _workout(day, sets, number):
            workout = Workout.objects.create(name='workout', date=dt(*day, tzinfo=pytz.UTC), owner=self.user, visibility='PR')
            ExType = Exercise.objects.get(name='Pushups')
            ExerciseInstance.objects.create(workout=workout, exercise=ExType, sets=sets, number=number)

        _workout((2021, 1, 1), 1, 1)
        _workout((2021, 1, 2), 1, 1)
        _workout((2021, 1, 5), 1, 5)
        _workout((2021, 1, 10), 5, 10)
        _workout((2021, 1, 11), 1, 1)

        data = self._request(self.user, day=(2021, 1, 1), days=30)
        self.assertEqual(data[0]['sets'], 1)
        self.assertEqual(data[0]['reps'], 1)

        data = self._request(self.user, day=(2021, 1, 2), days=30)
        self.assertEqual(data[0]['sets'], 2)
        self.assertEqual(data[0]['reps'], 2)

        data = self._request(self.user, day=(2021, 1, 5), days=30)
        self.assertEqual(data[0]['sets'], 3)
        self.assertEqual(data[0]['reps'], 7)

        data = self._request(self.user, day=(2021, 1, 10), days=30)
        self.assertEqual(data[0]['sets'], 8)
        self.assertEqual(data[0]['reps'], 57)

        data = self._request(self.user, day=(2021, 1, 11), days=30)
        self.assertEqual(data[0]['sets'], 9)
        self.assertEqual(data[0]['reps'], 58)

        data = self._request(self.user, day=(2021, 1, 11), days=2)
        self.assertEqual(data[0]['sets'], 6)
        self.assertEqual(data[0]['reps'], 51)

        data = self._request(self.user, day=(2021, 1, 11), days=1)
        self.assertEqual(data[0]['sets'], 1)
        self.assertEqual(data[0]['reps'], 1)

    def test_permissions(self):
        """You should only see your own statistics"""
        workout = Workout.objects.create(name='workout', date=dt(2021, 1, 1, tzinfo=pytz.UTC), owner=self.user, visibility='PR')
        ExType = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')
        ExerciseInstance.objects.create(workout=workout, exercise=ExType, sets=1, number=1)

        data = self._request(self.user, day=(2021, 1, 5), days=30)
        self.assertEqual(len(data), 1)

        data = self._request(self.other, day=(2021, 1, 5), days=30)
        self.assertEqual(len(data), 0)

    def test_types(self):
        """Test that different types are aggregated separately"""
        type1 = Exercise.objects.create(name='type1', description='type1', unit='type1')
        type2 = Exercise.objects.create(name='type2', description='type2', unit='type2')

        def _workout(day, sets, number, ExType):
            workout = Workout.objects.create(name='workout', date=dt(*day, tzinfo=pytz.UTC), owner=self.user, visibility='PR')
            ExerciseInstance.objects.create(workout=workout, exercise=ExType, sets=sets, number=number)

        def _get(data, name):
            return [d for d in data if d['name'] == name][0]

        _workout((2021, 1, 1), 1, 1, type1)
        _workout((2021, 1, 3), 5, 10, type1)
        _workout((2021, 1, 5), 3, 5, type1)

        _workout((2021, 1, 2), 2, 2, type2)
        _workout((2021, 1, 3), 4, 2, type2)
        _workout((2021, 1, 4), 8, 2, type2)

        data = self._request(self.user, day=(2021, 1, 1), days=30)
        self.assertEqual(len(data), 1)
        self.assertEqual(_get(data, 'type1')['sets'], 1)
        self.assertEqual(_get(data, 'type1')['reps'], 1)

        data = self._request(self.user, day=(2021, 1, 2), days=30)
        self.assertEqual(len(data), 2)
        self.assertEqual(_get(data, 'type1')['sets'], 1)
        self.assertEqual(_get(data, 'type1')['reps'], 1)
        self.assertEqual(_get(data, 'type2')['sets'], 2)
        self.assertEqual(_get(data, 'type2')['reps'], 4)

        data = self._request(self.user, day=(2021, 1, 5), days=30)
        self.assertEqual(len(data), 2)
        self.assertEqual(_get(data, 'type1')['sets'], 9)
        self.assertEqual(_get(data, 'type1')['reps'], 66)
        self.assertEqual(_get(data, 'type2')['sets'], 14)
        self.assertEqual(_get(data, 'type2')['reps'], 28)


class CalenderTests(TestCase):
    def setUp(self):
        user = User.objects.create(username='test_user')
        Workout.objects.create(name='W-feb1', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=user, visibility='PU')
        Workout.objects.create(name='W-mar1', date=dt(2021, 3, 5, tzinfo=pytz.UTC), owner=user, visibility='PU')
        Workout.objects.create(name='W-mar2', date=dt(2021, 3, 5, tzinfo=pytz.UTC), owner=user, visibility='PU')
        Workout.objects.create(name='W-apr1', date=dt(2021, 4, 5, tzinfo=pytz.UTC), owner=user, visibility='PU')

    # pylint: disable=R0201
    def _request(self, url, method, jsonData=None):
        factory = APIRequestFactory()
        user = User.objects.get(username='test_user')
        view = views.CalenderView.as_view()
        if method == 'get':
            request = factory.get(url)
        elif method == 'post':
            request = factory.post(url, jsonData, 'json')
        force_authenticate(request, user=user)
        response = view(request)
        if 'render' in dir(response):
            response.render()
        return response

    def _run_list_test(self, month, expected):
        response = self._request(f'/api/calender/?month={month}', 'get')
        data = json.loads(response.content)
        self.assertEqual(len(data['results']), len(expected))
        for w in data['results']:
            self.assertIn(w['name'], expected)

    def test_list_1(self):
        """Test listing results from calender"""
        self._run_list_test('2021-01', [])

    def test_list_2(self):
        """Test listing results from calender"""
        self._run_list_test('2021-02', ["W-feb1"])

    def test_list_3(self):
        """Test listing results from calender"""
        self._run_list_test('2021-03', ["W-mar1", "W-mar2"])

    def test_list_4(self):
        """Test listing results from calender"""
        self._run_list_test('2021-04', ["W-apr1"])

    def test_list_5(self):
        """Test listing results from calender"""
        self._run_list_test('2021-05', [])

    def test_create_1(self):
        """Test creating planned workout"""
        response = self._request('/api/calender/', 'post', {'name': 'new workout', 'date': '2021-05-01T00:00:00'})
        self.assertEqual(response.status_code, 200)
        self._run_list_test('2021-05', ['new workout'])


# pylint: disable=R0902 too-many-instance-attributes
class WorkoutPermissionsTests(TestCase):
    def setUp(self):
        self.coach = User.objects.create(username='coach')
        self.user = User.objects.create(username='user', coach=self.coach)
        self.other = User.objects.create(username='other')

        self.pub_w = Workout.objects.create(name='pub', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='PU')
        self.coach_w = Workout.objects.create(name='coach', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='CO')
        self.priv_w = Workout.objects.create(name='priv', date=dt(2021, 2, 5, tzinfo=pytz.UTC), owner=self.user, visibility='PR')

        self.ex = Exercise.objects.create(name='Pushups', description='Pushups', unit='Pushups')

        self.pub_ex = ExerciseInstance.objects.create(workout=self.pub_w, exercise=self.ex, sets=5, number=10)
        self.coach_ex = ExerciseInstance.objects.create(workout=self.coach_w, exercise=self.ex, sets=5, number=10)
        self.priv_ex = ExerciseInstance.objects.create(workout=self.priv_w, exercise=self.ex, sets=5, number=10)

    # pylint: disable=R0201,E1121 no-self-use, too-many-function-args
    def _req(self, user, method='get', data=None):
        if data is None:
            data = {}
        factory = APIRequestFactory()
        m = getattr(factory, method)
        if data:
            req = m('/', data, 'json')
        else:
            req = m('/')
        if user:
            force_authenticate(req, user)

        @api_view([method.upper()])
        def capture(req, rec):
            """dummy capture view to convert the django request to an api request"""
            rec[0] = req
            return Response({})

        rec = [0]
        capture(req, rec)
        return rec[0]

    def test_is_owner(self):
        """Checks whether the requesting user is also the owner of the existing object"""
        perm = IsOwner()
        view = None

        self.assertTrue(perm.has_object_permission(self._req(self.user), view, self.pub_w))
        self.assertFalse(perm.has_object_permission(self._req(self.coach), view, self.pub_w))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.pub_w))

    def test_is_owner_of_workout_new(self):
        """Checks whether the requesting user is also the owner of the new object"""
        perm = IsOwnerOfWorkout()
        view = None

        data = {'workout': f'{self.pub_w.id}/'}

        self.assertTrue(perm.has_permission(self._req(self.user, 'post', data), view))
        self.assertFalse(perm.has_permission(self._req(self.coach, 'post', data), view))
        self.assertFalse(perm.has_permission(self._req(self.other, 'post', data), view))

    def test_is_owner_of_workout_existing(self):
        """Checks whether the requesting user is also the owner of the existing object"""
        perm = IsOwnerOfWorkout()
        view = None

        self.assertTrue(perm.has_object_permission(self._req(self.user), view, self.pub_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.coach), view, self.pub_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.pub_ex))

    def test_is_coach_and_visible_to_coach(self):
        """Checks whether the requesting user is the existing object's owner's coach
        and whether the object (workout) has a visibility of Public or Coach.
        """
        perm = IsCoachAndVisibleToCoach()
        view = None

        self.assertTrue(perm.has_object_permission(self._req(self.coach), view, self.pub_w))
        self.assertTrue(perm.has_object_permission(self._req(self.coach), view, self.coach_w))
        self.assertFalse(perm.has_object_permission(self._req(self.coach), view, self.priv_w))

        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.pub_w))
        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.coach_w))
        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.priv_w))

        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.pub_w))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.coach_w))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.priv_w))

    def test_is_coach_of_workout_and_visible_to_coach(self):
        """Checks whether the requesting user is the existing workout's owner's coach
        and whether the object has a visibility of Public or Coach.
        """
        perm = IsCoachOfWorkoutAndVisibleToCoach()
        view = None

        self.assertTrue(perm.has_object_permission(self._req(self.coach), view, self.pub_ex))
        self.assertTrue(perm.has_object_permission(self._req(self.coach), view, self.coach_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.coach), view, self.priv_ex))

        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.pub_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.coach_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.user), view, self.priv_ex))

        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.pub_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.coach_ex))
        self.assertFalse(perm.has_object_permission(self._req(self.other), view, self.priv_ex))

    def test_is_public(self):
        """Checks whether the object (workout) has visibility of Public."""
        perm = IsPublic()
        view = None
        req = None

        self.assertTrue(perm.has_object_permission(req, view, self.pub_w))
        self.assertFalse(perm.has_object_permission(req, view, self.coach_w))
        self.assertFalse(perm.has_object_permission(req, view, self.priv_w))

    def test_is_workout_public(self):
        """Checks whether the object's workout has visibility of Public."""
        perm = IsWorkoutPublic()
        view = None
        req = None

        self.assertTrue(perm.has_object_permission(req, view, self.pub_ex))
        self.assertFalse(perm.has_object_permission(req, view, self.coach_ex))
        self.assertFalse(perm.has_object_permission(req, view, self.priv_ex))

    def test_is_read_only(self):
        """Checks whether the HTTP request verb is only for retrieving data (GET, HEAD, OPTIONS)"""
        methods = ['get', 'post', 'put', 'patch', 'delete', 'head', 'options']
        read = ['get', 'head', 'options']

        perm = IsReadOnly()
        view = None
        obj = None

        for method in methods:
            self.assertEqual(perm.has_object_permission(self._req(self.user, method), view, obj), method in read)


class TemplateTests(TestCase):
    def setUp(self):
        self.coach = User.objects.create(username='coach')
        self.user = User.objects.create(username='user', coach=self.coach)
        self.other = User.objects.create(username='other')

    def _request(self, TempList=False, TempId=None):
        factory = APIRequestFactory()
        view = None
        response = None

        if not TempList:
            request = factory.get(f'/api/templates/{TempId}/')
            force_authenticate(request, user=self.user)
            view = views.TemplateDetail.as_view()
            response = view(request, pk=TempId)
        else:
            request = factory.get('/api/templates/')
            force_authenticate(request, user=self.user)
            view = views.TemplateList.as_view()
            response = view(request)

        if 'render' in dir(response):
            response.render()
        return response.data

    def test_create_template(self):
        """Checks whether a template is created correctly or not."""
        template = Template.objects.create(name='test_template', owner=self.user, notes='hello', visibility='PU')

        template_response = self._request(False, template.id)

        self.assertTrue(template.id == template_response['id'])
        self.assertTrue(str(template.owner) == template_response['owner_username'])
        self.assertTrue(template.name == template_response['name'])
        self.assertTrue(template.notes == template_response['notes'])
        self.assertTrue(template.visibility == template_response['visibility'])

    def test_load_templates(self):
        """Checks whether the right amount of templates are loaded or not."""

        Template.objects.create(name='test_template_1', owner=self.user, notes='hello', visibility='PU')
        Template.objects.create(name='test_template_2', owner=self.user, notes='hello', visibility='PU')
        Template.objects.create(name='test_template_3', owner=self.user, notes='hello', visibility='PU')

        template_list_response = self._request(True)
        self.assertTrue(len(template_list_response['results']) == 3)
