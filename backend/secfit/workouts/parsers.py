"""Contains custom parsers for serializers from the workouts Django app
"""
import json
from contextlib import suppress
from rest_framework import parsers


def tryParseJson(value):
    if isinstance(value, str) and ("{" in value or "[" in value):
        with suppress(ValueError):
            return json.loads(value)
    return value


# Thanks to https://stackoverflow.com/a/50514630
# pylint: disable=R0903 too-few-public-methods
class MultipartJsonParser(parsers.MultiPartParser):
    """
    Parser for serializing multipart data containing both files and JSON.
    """

    def parse(self, stream, media_type=None, parser_context=None):
        result = super().parse(stream, media_type=media_type, parser_context=parser_context)

        data = {key: tryParseJson(value) for key, value in result.data.items()}
        files = result.files.getlist("files")
        new_files = {"files": [{'file': file} for file in files]}

        return parsers.DataAndFiles(data, new_files)
