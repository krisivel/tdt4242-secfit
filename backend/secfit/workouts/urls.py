from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from workouts import views

urlpatterns = format_suffix_patterns(
    [
        path("", views.api_root),
        path("api/calender/", views.CalenderView.as_view()),

        path("api/workouts/", views.WorkoutList.as_view(), name="workout-list"),
        path("api/workouts/<int:pk>/", views.WorkoutDetail.as_view(), name="workout-detail",),

        path("api/templates/", views.TemplateList.as_view(), name="template-list"),
        path("api/templates/<int:pk>/", views.TemplateDetail.as_view(), name="template-detail",),

        path("api/exercises/", views.ExerciseList.as_view(), name="exercise-list"),
        path("api/exercises/<int:pk>/", views.ExerciseDetail.as_view(), name="exercise-detail",),

        path("api/exercise-instances/", views.ExerciseInstanceList.as_view(), name="exercise-instance-list",),
        path("api/exercise-instances/<int:pk>/", views.ExerciseInstanceDetail.as_view(), name="exerciseinstance-detail",),

        path("api/workout-files/", views.WorkoutFileList.as_view(), name="workout-file-list",),
        path("api/workout-files/<int:pk>/", views.WorkoutFileDetail.as_view(), name="workoutfile-detail",),

        path("api/template-exercise-instances/", views.TemplateExerciseInstanceList.as_view(), name="template-exercise-instance-list",),
        path("api/template-exercise-instances/<int:pk>/", views.TemplateExerciseInstanceDetail.as_view(), name="templateexerciseinstance-detail",),

        path("api/template-workout-files/", views.TemplateWorkoutFileList.as_view(), name="template-workout-file-list",),
        path("api/template-workout-files/<int:pk>/", views.TemplateWorkoutFileDetail.as_view(), name="template-workoutfile-detail",),

        path("", include("users.urls")),
        path("", include("comments.urls")),

        path("api/auth/", include("rest_framework.urls")),
        path("api/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
        path("api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
        path("api/stats/", views.ExerciseStats.as_view(), name="stats"),
    ]
)
