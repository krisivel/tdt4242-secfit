"""Serializers for the workouts application
"""
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from workouts.models import (Exercise, ExerciseInstance, Template,
                             TemplateExerciseInstance, TemplateWorkoutFile,
                             Workout, WorkoutFile)


class ExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an ExerciseInstance. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, exercise, sets, number, workout

    Attributes:
        workout:    The associated workout for this instance, represented by a hyperlink
    """

    workout = HyperlinkedRelatedField(queryset=Workout.objects.all(), view_name="workout-detail", required=False)

    class Meta:
        model = ExerciseInstance
        fields = ["url", "id", "exercise", "sets", "number", "workout"]


class WorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a WorkoutFile. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, file, workout

    Attributes:
        owner:      The owner (User) of the WorkoutFile, represented by a username. ReadOnly
        workout:    The associate workout for this WorkoutFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(queryset=Workout.objects.all(), view_name="workout-detail", required=False)

    class Meta:
        model = WorkoutFile
        fields = ["url", "id", "owner", "file", "workout"]

    def create(self, validated_data):
        return WorkoutFile.objects.create(**validated_data)


def update_WorkoutFiles(instance, validated_data):
    if "files" in validated_data:
        files_data = validated_data.pop("files")
        files = instance.files.all()

        for file, file_data in zip(files, files_data):
            file.file = file_data.get("file", file.file)

        # if new files have been added, creating new WorkoutFiles
        for file in files_data[len(files):]:
            WorkoutFile.objects.create(workout=instance, owner=instance.owner, file=file.get("file"))

        # if files have been removed, delete WorkoutFiles
        for file in files[:len(files_data)]:
            file.delete()


def update_ExerciseInstances(instance, validated_data):
    ex_insts_new = validated_data.pop("exercise_instances")
    ex_insts = instance.exercise_instances.all()

    # This updates existing exercise instances without adding or deleting object.
    for ex_inst_old, ex_inst_new in zip(ex_insts, ex_insts_new):
        ex_inst_old.exercise = ex_inst_new.get("exercise", ex_inst_old.exercise)
        ex_inst_old.number = ex_inst_new.get("number", ex_inst_old.number)
        ex_inst_old.sets = ex_inst_new.get("sets", ex_inst_old.sets)
        ex_inst_old.save()

    # if new exercise instances have been added to the workout, then create them
    for new in ex_insts_new[len(ex_insts):]:
        ExerciseInstance.objects.create(workout=instance, **new)

    # if exercise instances have been removed from the workout, then delete them
    for old in ex_insts[len(ex_insts_new):]:
        old.delete()


class WorkoutSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Workout. Hyperlinks are used for relationships by default.

    This serializer specifies nested serialization since a workout consists of WorkoutFiles
    and ExerciseInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       exercise_instances, files

    Attributes:
        owner_username:     Username of the owning User
        exercise_instance:  Serializer for ExericseInstances
        files:              Serializer for WorkoutFiles
    """

    owner_username = serializers.SerializerMethodField()
    exercise_instances = ExerciseInstanceSerializer(many=True, required=True)
    files = WorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Workout
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "exercise_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, WorkoutFiles, and a Workout.

        This is needed to iterate over the files and exercise instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and exercise_instances

        Returns:
            Workout: A newly created Workout
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        workout = Workout.objects.create(**validated_data)

        for exercise_instance_data in exercise_instances_data:
            ExerciseInstance.objects.create(workout=workout, **exercise_instance_data)
        for file_data in files_data:
            WorkoutFile.objects.create(workout=workout, owner=workout.owner, file=file_data.get("file"))

        return workout

    def update(self, instance, validated_data):
        """Custom logic for updating a Workout with its ExerciseInstances and Workouts.

        This is needed because each object in both exercise_instances and files must be iterated
        over and handled individually.

        Args:
            instance (Workout): Current Workout object
            validated_data: Contains data for validated fields

        Returns:
            Workout: Updated Workout instance
        """

        instance.name = validated_data.get("name", instance.name)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get("visibility", instance.visibility)
        instance.date = validated_data.get("date", instance.date)
        instance.save()

        update_ExerciseInstances(instance, validated_data)
        update_WorkoutFiles(instance, validated_data)

        return instance

    # pylint: disable=R0201
    def get_owner_username(self, obj):
        """Returns the owning user's username

        Args:
            obj (Template): Current Template

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class TemplateWorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a TemplateWorkoutFile. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, file, template

    Attributes:
        owner:      The owner (User) of the TemplateWorkoutFile, represented by a username. ReadOnly
        template:    The associate template for this TemplateWorkoutFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    template = HyperlinkedRelatedField(
        queryset=Template.objects.all(), view_name="template-detail", required=False
    )

    class Meta:
        model = TemplateWorkoutFile
        fields = ["url", "id", "owner", "template_file", "template"]

    def create(self, validated_data):
        return TemplateWorkoutFile.objects.create(**validated_data)


class TemplateExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an ExerciseInstance. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, exercise, sets, number, template

    Attributes:
        template:    The associated template for this instance, represented by a hyperlink
    """

    template = HyperlinkedRelatedField(queryset=Template.objects.all(), view_name="template-detail", required=False)

    class Meta:
        model = TemplateExerciseInstance
        fields = ["url", "id", "exercise", "sets", "number", "template"]


class TemplateSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Template. Hyperlinks are used for relationships by default.

    This serializer specifies nested serialization since a template consists of TemplateFiles
    and ExerciseInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       template_exercise_instances, files

    Attributes:
        owner_username:     Username of the owning User
        template_exercise_instance:  Serializer for TemplateExericseInstances
        files:              Serializer for TemplateFiles
    """

    owner_username = serializers.SerializerMethodField()
    template_exercise_instances = TemplateExerciseInstanceSerializer(many=True, required=True)
    template_files = TemplateWorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Template
        fields = [
            "url",
            "id",
            "name",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "template_exercise_instances",
            "template_files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, TemplateFiles, and a Template.

        This is needed to iterate over the files and exercise instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and template_exercise_instances

        Returns:
            Template: A newly created Template
        """
        template_exercise_instances_data = validated_data.pop("template_exercise_instances")
        files_data = []
        if "template_files" in validated_data:
            files_data = validated_data.pop("template_files")

        template = Template.objects.create(**validated_data)

        for template_exercise_instance_data in template_exercise_instances_data:
            TemplateExerciseInstance.objects.create(template=template, **template_exercise_instance_data)
        for file_data in files_data:
            TemplateWorkoutFile.objects.create(template=template, owner=template.owner, file=file_data.get("file"))

        return template

    # pylint: disable=R0201
    def get_owner_username(self, obj):
        """Returns the owning user's username

        Args:
            obj (Template): Current Template

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Exercise. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, description, unit, instances

    Attributes:
        instances:  Associated exercise instances with this Exercise type. Hyperlinks.
    """

    instances = serializers.HyperlinkedRelatedField(many=True, view_name="exerciseinstance-detail", read_only=True)

    class Meta:
        model = Exercise
        fields = ["url", "id", "name", "description", "unit", "instances"]
