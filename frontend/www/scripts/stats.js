async function fetchStatistics (days) {
    const response = await sendRequest('GET', `${HOST}/api/stats/?days=${days}`);

    if (response.ok) {
        const exercises = await response.json();

        const container = document.getElementById('div-content');
        container.innerHTML = '';
        for (const id in exercises) {
            const exercise = exercises[id];
            const templateWorkout = document.querySelector('#template-workout');
            const cloneWorkout = templateWorkout.content.cloneNode(true);

            const aExercise = cloneWorkout.querySelector('a');

            const h5 = aExercise.querySelector('h5');
            h5.textContent = exercise.name;

            const type = `${exercise.unit} of ${exercise.name.toLowerCase()}`;

            const content = `<ul>
            <li>Total: <b>${exercise.reps}</b> ${type}</li>
            <li>Total sets: <b>${exercise.sets}</b></li>
            <li>Average ${type} per set: <b>${(exercise.reps / exercise.sets).toFixed(2)}</b></li>
            </ul>`;

            const table = aExercise.querySelector('table');
            const rows = table.querySelectorAll('tr');
            rows[0].querySelectorAll('td')[1].innerHTML = content;

            container.appendChild(aExercise);
        }
        if (exercises.length === 0) {
            const templateWorkout = document.querySelector('#template-workout');
            const cloneWorkout = templateWorkout.content.cloneNode(true);
            const aExercise = cloneWorkout.querySelector('a');
            const h5 = aExercise.querySelector('h5');
            h5.textContent = 'No statistics to show';
            const table = aExercise.querySelector('table');
            const rows = table.querySelectorAll('tr');
            rows[0].querySelectorAll('td')[1].textContent = 'Start logging some workouts to see your statistics here!';
            container.appendChild(aExercise);
        }
    } else {
        const data = await response.json();
        const alert = createAlert('Could not retrieve exercise stats!', data);
        document.body.prepend(alert);
    }
    return response;
}

window.addEventListener('DOMContentLoaded', async () => {
    const days = document.querySelector('#daySelector');
    days.addEventListener('input', async () => await fetchStatistics(days.value));

    await fetchStatistics(days.value);
});
