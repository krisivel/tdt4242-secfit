async function fetchCalender (queryMonth) {
    const response = await sendRequest('GET', `${HOST}/api/calender/?month=${queryMonth}`);

    document.querySelector('#current-view').textContent = queryMonth;

    let workouts = [];
    const data = await response.json();

    if (response.ok) {
        workouts = data.results;
    } else {
        const alert = createAlert('Could not retrieve exercise types!', data);
        document.body.prepend(alert);
    }

    const month = new Date(queryMonth + '-1');

    const first = new Date(month.getFullYear(), month.getMonth(), 1);
    first.setDate(2 - (first.getDay() + 7) % 8); // setDate(1) first of month, we want to get to where getDay() == 1 (getDay() == 0 => Sunday)
    while (first.getDay() !== 1) {
        first.setDate(first.getDate() - 1);
    }

    const weekTemplate = document.querySelector('#week-row-template').content.firstElementChild;
    const dayTemplate = document.querySelector('#day-template').content.firstElementChild;
    const workoutTemplate = document.querySelector('#workout-template').content.firstElementChild;
    const container = document.querySelector('#div-content');
    container.innerHTML = '';

    for (let week = 0; week < 6; week++) {
        const weekNode = weekTemplate.cloneNode(true);
        container.appendChild(weekNode);
        for (let day = 0; day < 7; day++) {
            const date = new Date(first);
            date.setDate(date.getDate() + week * 7 + day);

            const dayNode = dayTemplate.cloneNode(true);

            const dayTitle = dayNode.querySelector('.title');
            dayTitle.textContent = date.toLocaleDateString();

            const dayWorkouts = dayNode.querySelector('.workouts');
            workouts.forEach(ex => {
                if (sameDate(new Date(ex.date), date)) {
                    const workoutNode = workoutTemplate.cloneNode(true);
                    dayWorkouts.appendChild(workoutNode);
                    workoutNode.textContent = ex.name;
                }
            });

            if (date.getMonth() !== month.getMonth()) {
                dayNode.classList.add('other');
                if (day === 0 && week > 0) {
                    week = 1000;
                    container.removeChild(weekNode);
                    break;
                }
            }
            if (sameDate(date, new Date())) {
                dayNode.classList.add('today');
            } else if (date.getTime() > new Date().getTime()) {
                dayNode.classList.add('future');
            }

            weekNode.appendChild(dayNode);
            if (dayNode.matches('div.today, div.future') && !dayNode.matches('div.other')) {
                dayNode.addEventListener('dblclick', async e => {
                    const name = prompt('Workout name:');
                    if (!name) return;
                    await sendRequest('POST', '/api/calender/', { name, date }, 'application/json');
                    console.log('refreshing calender');
                    await fetchCalender(queryMonth);
                });
            }
        }
    }

    return response;
}

function sameDate (d1, d2) {
    return dateOnly(d1).getTime() === dateOnly(d2).getTime();
}

function dateOnly (date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

async function move (cur, n) {
    cur.setMonth(cur.getMonth() + n);
    await fetchCalender(`${cur.getFullYear()}-${cur.getMonth() + 1}`);
}

window.addEventListener('DOMContentLoaded', async () => {
    const backButton = document.querySelector('#btn-back');
    const nextButton = document.querySelector('#btn-next');
    const date = new Date();
    backButton.addEventListener('click', async () => await move(date, -1));
    nextButton.addEventListener('click', async () => await move(date, 1));

    move(date, 0);
});
