async function fetchObjects (fetchedObjects, ordering) {
    // For loading either templates or workouts

    var fetchFrom = `${HOST}/api/` + fetchedObjects;
    const currentUser = await getCurrentUser();
    var fetchedObject = fetchedObjects.slice(0, -1);

    if (ordering) {
        fetchFrom.concat(`/?ordering=${ordering}`);
    }
    const response = await sendRequest('GET', fetchFrom);

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        const data = await response.json();
        const objects = data.results;
        const container = document.getElementById('div-content');
        objects.forEach(object => {
            const templateObject = document.querySelector('#template-' + fetchedObject);
            const cloneObject = templateObject.content.cloneNode(true);

            const anObject = cloneObject.querySelector('a');

            const h5 = anObject.querySelector('h5');
            h5.textContent = object.name;

            const table = anObject.querySelector('table');
            const rows = table.querySelectorAll('tr');

            if (fetchedObjects === 'workouts') {
                anObject.href = `workout.html?id=${object.id}`;
                const localDate = new Date(object.date);

                // What to add to workout rows
                rows[0].querySelectorAll('td')[1].textContent = localDate.toLocaleDateString(); // Date
                rows[1].querySelectorAll('td')[1].textContent = localDate.toLocaleTimeString(); // Time
                rows[2].querySelectorAll('td')[1].textContent = object.owner_username; // Owner
                rows[3].querySelectorAll('td')[1].textContent = object.exercise_instances.length; // Exercises
            } else if (fetchedObjects === 'templates') {
                // What to add to template rows
                rows[0].querySelectorAll('td')[1].textContent = object.owner_username; // Owner
                rows[1].querySelectorAll('td')[1].textContent = object.template_exercise_instances.length; // Exercises

                // Adding delete button on your own templates
                if (object.owner === currentUser.url) {
                    const deleteButton = anObject.querySelector('#btn-delete-template');
                    deleteButton.className = deleteButton.className.replace(' hide', '');
                    deleteButton.addEventListener('click', () => deleteTemplate(object.id), false);
                }

                // Create workout from template button
                const createButton = anObject.querySelector('#btn-create-workout');
                createButton.addEventListener('click', () => createWorkoutFromTemplate(object.id), false);
            }

            container.appendChild(anObject);
        });
        return objects;
    }
}

function createWorkout () {
    window.location = 'workout.html';
}

function viewButton (object) {
    window.location = object + '.html';
}

async function deleteTemplate (id) {
    const response = await sendRequest('DELETE', `${HOST}/api/templates/${id}/`);
    if (!response.ok) {
        const data = await response.json();
        const alert = createAlert(`Could not delete template ${id}!`, data);
        document.body.prepend(alert);
    } else {
        window.location = 'templates.html';
    }
}

async function createWorkoutFromTemplate (id) {
    window.location = `workout.html?tid=${id}`;
}

window.addEventListener('DOMContentLoaded', async () => {
    const currentUser = await getCurrentUser();
    var object = '';
    var objects = '';
    var objectCounterPart = '';
    let ordering = '-date';

    if (window.location.href.indexOf('workouts') > -1) {
        // Loading procedures for workouts

        object = 'workout';
        objects = 'workouts';
        objectCounterPart = 'templates';

        const createButton = document.querySelector('#btn-create-workout');
        createButton.addEventListener('click', createWorkout);

        const urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('ordering')) {
            ordering = urlParams.get('ordering');
            if (ordering === 'name' || ordering === 'owner' || ordering === 'date') {
                const aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
                aSort.href = `?ordering=-${ordering}`;
            }
        }

        const currentSort = document.querySelector('#current-sort');
        currentSort.innerHTML = (ordering.startsWith('-') ? 'Descending' : 'Ascending') + ' ' + ordering.replace('-', '');

        if (ordering.includes('owner')) {
            ordering += '__username';
        }
    } else if (window.location.href.indexOf('templates') > -1) {
        // Loading procedures for templates

        object = 'template';
        objects = 'templates';
        objectCounterPart = 'workouts';
        ordering = '';
    }

    // Loading procedures for both objects

    const fetchedObjects = await fetchObjects(objects, ordering);

    const viewObjectCounterPartButton = document.querySelector('#btn-view-' + objectCounterPart);
    viewObjectCounterPartButton.addEventListener('click', () => viewButton(objectCounterPart), false);

    const tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let i = 0; i < tabEls.length; i++) {
        const tabEl = tabEls[i];
        tabEl.addEventListener('show.bs.tab', function (event) {
            const objectAnchors = document.querySelectorAll('.' + object);
            for (let j = 0; j < fetchedObjects.length; j++) {
                const fetchedObject = fetchedObjects[j];
                const objectAnchor = objectAnchors[j];

                switch (event.currentTarget.id) {
                case 'list-my-' + objects + '-list':
                    if (fetchedObject.owner === currentUser.url) {
                        objectAnchor.classList.remove('hide');
                    } else {
                        objectAnchor.classList.add('hide');
                    }
                    break;
                case 'list-athlete-' + objects + '-list':
                    if (currentUser.athletes && currentUser.athletes.includes(fetchedObject.owner)) {
                        objectAnchor.classList.remove('hide');
                    } else {
                        objectAnchor.classList.add('hide');
                    }
                    break;
                case 'list-public-workouts-list':
                    if (fetchedObject.visibility === 'PU') {
                        objectAnchor.classList.remove('hide');
                    } else {
                        objectAnchor.classList.add('hide');
                    }
                    break;
                default :
                    objectAnchor.classList.remove('hide');
                    break;
                }
            }
        });
    }
});
